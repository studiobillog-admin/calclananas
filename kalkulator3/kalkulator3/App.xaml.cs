﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace kalkulator3
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new XamarinCalculator.MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
